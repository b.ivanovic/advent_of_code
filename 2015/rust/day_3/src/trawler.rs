use crate::grid_builder;

/// Updates the current position of Santa, increasing the size of the grid
/// if the new position would be out of bounds (by using 'grid_builder'),
/// and delivers a present at the new location

/// Updates positions for both Santa and RoboSanta if a left column
/// is added
pub fn move_left<'a>(
    grid: &'a mut Vec<Vec<i32>>,
    position: &mut Vec<i32>,
    other_pos: &mut Vec<i32>) -> &'a mut Vec<Vec<i32>>  {

    match grid.get_mut((position[0] -1) as usize) {
        Some(_) => {
            position[0] -= 1;
        },
        None => {
            grid_builder::add_column_left(grid);
            other_pos[0] += 1;
        }
    }

    grid[position[0] as usize][position[1] as usize] += 1;

    return grid;
}

pub fn move_right<'a>(grid: &'a mut Vec<Vec<i32>>, position: &mut Vec<i32>) -> &'a mut Vec<Vec<i32>> {
    match grid.get_mut((position[0] + 1) as usize) {
        Some(_) => { 
            position[0] += 1;
        },
        None => { 
            grid_builder::add_column_right(grid);
            position[0] += 1;
        }
    }

    grid[position[0] as usize][position[1] as usize] += 1;

    return grid;
}

/// Updates positions for both Santa and RoboSanta if a top row
/// is added
pub fn move_up<'a>(
    grid: &'a mut Vec<Vec<i32>>,
    position: &mut Vec<i32>,
    other_pos: &mut Vec<i32>) -> &'a mut Vec<Vec<i32>> {

    match grid[position[0] as usize].get_mut((position[1] - 1) as usize) {
        Some(_) => { 
            position[1] -= 1;
        },
        None => { 
            grid_builder::add_row_top(grid);
            other_pos[1] += 1;
        }
    }

    grid[position[0] as usize][position[1] as usize] += 1;

    return grid;
}

pub fn move_down<'a>(grid: &'a mut Vec<Vec<i32>>, position: &mut Vec<i32>) -> &'a mut Vec<Vec<i32>> {
    match grid[position[0] as usize].get_mut((position[1] + 1) as usize) {
        Some(_) => { 
            position[1] += 1;
        },
        None => { 
            grid_builder::add_row_bottom(grid);
            position[1] += 1;
        }
    }

    grid[position[0] as usize][position[1] as usize] += 1;

    return grid;
}

pub fn count_houses(grid: Vec<Vec<i32>>) -> i32 {
    let mut count = 0;

    for column in grid {
        for value in column {
            if value != 0 { count += 1; }
        }
    }

    return count;
}
