/// If Santa moves to a position that would be out of bounds
/// of the grid, this mod will add a new row or column, depending
/// on the direction of the movement

pub fn add_row_top(grid: &mut Vec<Vec<i32>>) -> &mut Vec<Vec<i32>> {
    for column in grid.iter_mut() {
        column.insert(0, 0);
    }

    return grid;
}

pub fn add_row_bottom(grid: &mut Vec<Vec<i32>>) -> &mut Vec<Vec<i32>> {
    for column in grid.iter_mut() {
        column.push(0);
    }

    return grid;
}

pub fn add_column_left(grid: &mut Vec<Vec<i32>>) -> &mut Vec<Vec<i32>> {
    let grid_height: usize = grid[0].len();

    grid.insert(0, vec![0; grid_height]);

    return grid;
}
pub fn add_column_right(grid: &mut Vec<Vec<i32>>) -> &mut Vec<Vec<i32>> {
    let grid_height: usize = grid[0].len();

    grid.push(vec![0; grid_height]);

    return grid;
}
