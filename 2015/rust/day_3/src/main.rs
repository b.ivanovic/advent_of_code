mod file_parser;
mod trawler;
mod grid_builder;

fn main() {
    // setup
    let input_content: String = file_parser::parse_file("src/input.txt");
    let mut house_grid: Vec<Vec<i32>> = vec![vec![0; 1]; 1];
    let mut santa_position: Vec<i32> = vec![0, 0];
    let mut robosanta_position: Vec<i32> = vec![0, 0];
    let mut current_deliverer: SantaOrRoboSanta = SantaOrRoboSanta::Santa;

    // deliver a present at the starting location (0, 0) for both Santa and RoboSanta
    house_grid[santa_position[0] as usize][santa_position[1] as usize] += 1;
    house_grid[robosanta_position[0] as usize][robosanta_position[1] as usize] += 1;

    // goes through the instruction set and moves through the grid according to the
    // char supplied, as well as increasing the size of the grid when needed
    for instruction in input_content.chars() {
        match instruction {
            '<' => {
                if matches!(&current_deliverer, SantaOrRoboSanta::Santa) {
                    house_grid = trawler::move_left(
                        &mut house_grid, &mut santa_position, &mut robosanta_position).to_vec();
                    current_deliverer = SantaOrRoboSanta::RoboSanta;
                } else if matches!(&current_deliverer, SantaOrRoboSanta::RoboSanta) {
                    house_grid = trawler::move_left(
                        &mut house_grid, &mut robosanta_position, &mut santa_position).to_vec();
                    current_deliverer = SantaOrRoboSanta::Santa;
                }
            },
            '>' => {
                if matches!(&current_deliverer, SantaOrRoboSanta::Santa) {
                    house_grid = trawler::move_right(&mut house_grid, &mut santa_position).to_vec();
                    current_deliverer = SantaOrRoboSanta::RoboSanta;
                } else if matches!(&current_deliverer, SantaOrRoboSanta::RoboSanta) {
                    house_grid = trawler::move_right(&mut house_grid, &mut robosanta_position).to_vec();
                    current_deliverer = SantaOrRoboSanta::Santa;
                }
            },
            '^' => {
                if matches!(&current_deliverer, SantaOrRoboSanta::Santa) {
                    house_grid = trawler::move_up(
                        &mut house_grid, &mut santa_position, &mut robosanta_position).to_vec();
                    current_deliverer = SantaOrRoboSanta::RoboSanta;
                } else if matches!(&current_deliverer, SantaOrRoboSanta::RoboSanta) {
                    house_grid = trawler::move_up(
                        &mut house_grid, &mut robosanta_position, &mut santa_position).to_vec();
                    current_deliverer = SantaOrRoboSanta::Santa;
                }
            },
            'v' => {
                if matches!(&current_deliverer, SantaOrRoboSanta::Santa) {
                    house_grid = trawler::move_down(&mut house_grid, &mut santa_position).to_vec();
                    current_deliverer = SantaOrRoboSanta::RoboSanta;
                } else if matches!(&current_deliverer, SantaOrRoboSanta::RoboSanta) {
                    house_grid = trawler::move_down(&mut house_grid, &mut robosanta_position).to_vec();
                    current_deliverer = SantaOrRoboSanta::Santa;
                }
            },
            _ => ()
        }
    }

    println!("Total number of houses that received a present is: {}", trawler::count_houses(house_grid));

}

#[derive(Debug)]
enum SantaOrRoboSanta {
    Santa,
    RoboSanta
}
