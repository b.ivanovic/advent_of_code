use std::fs;

/// Parses the file at supplied path and returns a String

pub fn parse_file(file_path: &str) -> String {

    let file_content = fs::read_to_string(file_path)
        .expect("Cannot read file at supplied file_path.");

    return file_content;

}
