use std::fs;
mod helpers;

fn main() {
    let contents: String = String::from(
        fs::read_to_string("src/input.txt")
            .expect("Cannot read file"));

    let mut total_area: u32 = 0;
    let mut total_ribbon_length: u32 = 0;
    let mut current_row: Vec<u32>;

    // split file by line
    let all_rows = contents.split("\n");
    let rows_vec: Vec<&str> = all_rows.collect();

    // spit line by x
    for x in rows_vec {
        if x.is_empty() { continue; };
        current_row = helpers::get_row_value(x);
        // calculate the wrapping paper area and add to total
        total_area += helpers::calculate_wrapping_area(
            current_row[0],
            current_row[1],
            current_row[2]);

        // calculate the ribbon length and add to total
        total_ribbon_length += helpers::calculate_ribbon_length(x);
    }

    println!("Total area for all wrappings: {} sqare feet", total_area);
    println!("Total ribbon for all wrappings: {} feet", total_ribbon_length);
}

