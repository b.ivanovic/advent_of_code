use std::cmp;

/// Gets the valus for a single row in the file
pub fn get_row_value(row: &str) -> Vec<u32> {
    let row_iter = row.split("x");
    let mut row_values: Vec<u32> = vec![];

    for v in row_iter {
        if v.len() == 0 { return vec![]; };
        row_values.push(v.parse().unwrap());
    }

    return row_values;
}

/// Gets the minimum of x int values
pub fn get_min(args: &[u32]) -> u32 {
    let mut min: u32 = std::u32::MAX;
    for a in args {
        min = cmp::min(min, *a);
    }

    return min;
}

/// Calculates a right ractangular prism area +
/// the smallest side area
pub fn calculate_wrapping_area(l: u32, w: u32, h: u32) -> u32 {
    let sides1: u32 = 2 * l * w;
    let sides2: u32 = 2 * w * h;
    let sides3: u32 = 2 * h * l;

    let area: u32 = sides1 + sides2 + sides3;

    let min_value = get_min(&[sides1 / 2, sides2 / 2, sides3 / 2]);

    let to_return = area + min_value;

    return to_return;
}

/// Calculates the legth of ribbon needed for present and bow
pub fn calculate_ribbon_length(dimensions :&str) -> u32 {
    let mut box_dimensions = get_row_value(dimensions);

    // a * b * c
    let bow_length: u32 = box_dimensions[0]
    * box_dimensions[1]
    * box_dimensions[2];

    // 2 * smallest + 2 * second smallest
    box_dimensions.sort();

    let ribbon_length: u32 = box_dimensions[0]
        + box_dimensions[0]
        + box_dimensions[1]
        + box_dimensions[1];

    return ribbon_length + bow_length;
}
