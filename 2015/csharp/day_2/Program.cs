﻿using AoC.Day2.Utils;

namespace AoC.Day2;

public class ElvenCalculator
{
    public static void Main()
    {
        Parser inputParser = new Parser();
        WrappingCalculator wrapCalc = new WrappingCalculator();
        RibbonCalculator ribbonCalc = new RibbonCalculator();

        var fileRows = inputParser.ReadFileRows("input.txt");

        int totalWrappingArea = 0;
        int totalRibbonLength = 0;

        foreach (string row in fileRows)
        {
            if (String.IsNullOrWhiteSpace(row)) continue;
            totalWrappingArea += wrapCalc.CalculateWrappingPaperArea(inputParser.ParseRowValues(row, 'x'));
        }

        foreach (string row in fileRows)
        {
            if (String.IsNullOrWhiteSpace(row)) continue;
            totalRibbonLength += ribbonCalc.GetRibbonLength(inputParser.ParseRowValues(row, 'x'));
        }

        System.Console.WriteLine("Total wrapping area: " + totalWrappingArea + " square feet");
        System.Console.WriteLine("Total ribbon length: " + totalRibbonLength + " feet");
    }
}
