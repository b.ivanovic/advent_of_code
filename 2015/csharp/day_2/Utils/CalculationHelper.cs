namespace AoC.Day2.Utils;

public class CalculationHelper
{
    public int GetMinValue(List<int> values)
    {
        return values.Min(v => v);
    }
}
