namespace AoC.Day2.Utils;

public class Parser
{
    public string[] ReadFileRows(string filePath)
    {
        return File.ReadAllLines(filePath);
    }

    public List<int> ParseRowValues(string row, char splitter)
    {
        var toReturn = new List<int>();
        var values = row.Split(splitter);

        foreach (var value in values)
        {
            toReturn.Add(Int32.Parse(value));
        }

        return toReturn;
    }
}
