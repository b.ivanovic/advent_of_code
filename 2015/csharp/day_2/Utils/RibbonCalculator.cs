namespace AoC.Day2.Utils;

public class RibbonCalculator
{
    public int GetRibbonLength(List<int> presentDimensions)
    {
        int ribbonLength = GetWrappingRibbonLength(presentDimensions);
        int bowLength = GetBowRibbonLength(presentDimensions);
        return ribbonLength + bowLength;
    }

    private int GetWrappingRibbonLength(List<int> presentDimensions)
    {
        presentDimensions.Sort();
        int toReturn = presentDimensions[0]
            + presentDimensions[0]
            + presentDimensions[1]
            + presentDimensions[1];

        return toReturn;
    }

    private int GetBowRibbonLength(List<int> presentDimensions)
    {
        int toReturn = presentDimensions[0]
            * presentDimensions[1]
            * presentDimensions[2];
        
        return toReturn;
    }
}
