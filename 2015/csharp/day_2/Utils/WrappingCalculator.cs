namespace AoC.Day2.Utils;

public class WrappingCalculator
{
    public int CalculateWrappingPaperArea(List<int> presentDimensions)
    {
        int l = presentDimensions[0];
        int w = presentDimensions[1];
        int h = presentDimensions[2];

        int side1 = 2 * l * w;
        int side2 = 2 * w * h;
        int side3 = 2 * h * l;

        List<int> allSides = new List<int>();
        allSides.Add(side1 / 2);
        allSides.Add(side2 / 2);
        allSides.Add(side3 / 2);

        CalculationHelper calc = new CalculationHelper();

        int smallestSide = calc.GetMinValue(allSides);

        return side1 + side2 + side3 + smallestSide;
    }
}
