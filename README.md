# advent_of_code

Solutions for the Advent of Code challenges in Rust and C#

## Directory structure

* base: [year]/[lang]/[day]
* lang specific:
    * rust - ./src/
    * c# - .
